import React, {Component} from 'react';

class ListItems extends Component {

  onCheckChange = (event) => {
    this.props.onCheckChange(event.target.id);
  }

  render() {
    let {items, filterText, status} = this.props;
    const itemList = items
      .filter(item => {
        return (
          item.text.toLowerCase().indexOf(filterText.toLowerCase()) !== -1
        )
      })
      .filter(item => {
        if(status === -1) {
          return item;
        } else {
          return item.status === status;
        }
      })
      .map((item, index) => {
        return (
          <li key={index} id={item.id} status={item.status} onClick={this.onCheckChange.bind(this)}>{item.status} : {item.text}</li>
        );
      });

    return (
      <ul className="list">
        {itemList}
      </ul>
    );
  }
}

export default ListItems;