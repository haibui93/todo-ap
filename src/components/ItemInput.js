import React, {Component} from 'react';

class ItemInput extends Component {

  render() {
    return (
      <div className="items">
        <form onSubmit={this.props.handelSubmit}>
          <div className="form-group">
            <input 
              type="text"
              value={this.props.text}
              onChange={this.props.handleChange}
            />
          </div>

          <div className="form-group">
            <input type="submit" value="Add Item" />
          </div>
        </form>
      </div>
    );
  }

}

export default ItemInput;