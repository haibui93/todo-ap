import React, {Component} from 'react';

class Search extends Component {

  filterUpdate = (event) => {
    let val = this.refs.myValue.value;
    this.props.filterUpdate(val);
  }

  render() {
    return(
      <div className="form-search">
        <h4>Input the key search: ... </h4>
        <form>
          <input 
            type="text"
            ref="myValue"
            placeholder="Type to filter ..." 
            onChange={this.filterUpdate.bind(this)} />
        </form>
      </div>
    );
  }
}

export default Search;