import React, {Component} from 'react';

class Filter extends Component {

  onFilter = (event) => {
    let val = event.target.id;
    this.props.onFilter(val);
  }

  render() {
    return (
      <div className="item-filter">
        <ul>
          <li onClick={this.onFilter.bind(this)} id="all">ALL</li>
          <li onClick={this.onFilter.bind(this)} id="checked">Checked</li>
          <li onClick={this.onFilter.bind(this)} id="unCheck">Uncheck</li>
        </ul>
      </div>
    );
  };

}

export default Filter;