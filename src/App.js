import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import InputItem from './components/ItemInput';
import ListItems from './components/ListItems';
import Filter from './components/Filter';
import Search from './components/Search';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      text: '',
      count: 0,
      filterText: '',
      stt: -1
    };
  }

  handleChange = (event) => {
    let changeText = event.target.value;
    this.setState({
      text: changeText
    });
  }

  handelSubmit = (event) => {
    event.preventDefault();
    if(!this.state.text.length) {
      return;
    }
    let newItem = {
      text: this.state.text,
      id: Date.now(),
      status: 0
    };
    
    this.setState(prevState => ({
      items: prevState.items.concat(newItem),
      text: ''
    }));
  }

  onCheckChange = (id) => {
    let idx = this.state.items.findIndex((item) => {
      return item.id === parseInt(id, 10);
    });
    let temp = this.state.items;
    if(idx >= 0) {
      temp[idx].status === 0 ? temp[idx].status = 1 : temp[idx].status = 0;
    }
    this.setState({
      items: temp,
      count: this.state.items.filter(item => item.status === 1).length
    });
  }

  onFilter = (value) => {
    let stt = this.state.stt;
    switch(value) {
      case 'all': stt = -1;break;
      case 'checked': stt = 1;break;
      case 'unCheck': stt = 0;break;
      default: stt = '';break;
    }
    this.setState({
      stt: stt
    })
  }

  filterUpdate = (value) => {
    this.setState({
      filterText: value
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        
        <div className="listItems">
          <p>Total: {this.state.items.length}</p>
          <p>Total Checked: {this.state.count}</p>
          <p>Total UnCheck: {this.state.items.length - this.state.count}</p>
          
          <Search
            filterText={this.state.filterText}
            filterUpdate={this.filterUpdate}
          />

          <Filter
            onFilter={this.onFilter}
          />
          <ListItems 
            items={this.state.items}
            onCheckChange={this.onCheckChange}
            filterText={this.state.filterText}
            status={this.state.stt}
           />
          <InputItem
            text={this.state.text}
            handleChange={this.handleChange.bind(this)}
            handelSubmit={this.handelSubmit.bind(this)}
          />
        </div>

      </div>
    );
  }
}

export default App;
